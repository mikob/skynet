#ifndef SSLSERVER_H
#define SSLSERVER_H

#include <QTcpServer>
#include <QSslSocket>
#include <QSslKey>
#include <QList>

class SSLServer : public QTcpServer
{
    Q_OBJECT

public:
    SSLServer();
    QSslSocket *nextPendingConnection();
protected:
    // Override QTcpServer::incomingConnection()
    void incomingConnection(int socketDescriptor);
private:
    // Keep track of the sockets we have created
    QList<QSslSocket *> m_secureSocketList;
};

#endif // SSLSERVER_H
