#include <QDebug>
#include "sslserver.h"

SSLServer::SSLServer() : QTcpServer()
{

}

void SSLServer::incomingConnection(int socketDescriptor)
{
    // Create a new secure socket for each incoming connection
    QSslSocket *secureSocket = new QSslSocket;

    // Add to list so we can find it with nextConnection
    m_secureSocketList.append(secureSocket);

    // We need to read in the local certificate and the
    // private key we generated with openssl. 
    secureSocket->setLocalCertificate("cacert.pem");
    secureSocket->setPrivateKey("privkey.pem");

    // Make sure the certificate and private key are there
    if (secureSocket->localCertificate().isNull()) {
        qDebug() << "WARNING: Problem loading the local certificate" << endl;
    }
    if (secureSocket->privateKey().isNull()) {
        qDebug() << "WARNING: Problem loading the private key" << endl;
    }

    qDebug() << "Created the SSL socket, Read local cert. / private key files";

    secureSocket->setSocketDescriptor(socketDescriptor);

    secureSocket->startServerEncryption();
}

QSslSocket *SSLServer::nextPendingConnection()
{
    QSslSocket *secureSocket = NULL;
    if (m_secureSocketList.isEmpty()) {
        qDebug() << "Secure socket list is empty." << endl;
    } else {
        secureSocket = m_secureSocketList.last();
        m_secureSocketList.removeLast();
    }
    return secureSocket;
}
