#ifndef DATABASEINTERFACE_H
#define DATABASEINTERFACE_H
#include <QtCore>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

class DatabaseInterface
{
public:
    DatabaseInterface();

    // The eight primary functions of the server/database
    bool registerUser(const QString* username, const QString* encryptedPassword);
    bool changePassword(const QString* newPassword);
    bool validateUser(const QString* username, const QString* password);
    bool addNewLog(const QString* username, const QString* log);
    QString retrieveLogs(const QString* username);
	bool changePermissionsGroups(const QString* username, const QString* newGroups);
	bool changeMemberGroups(const QString* username, const QString* newGroups);
    QString getAllGroupData();

private:
	QSqlDatabase db;
    void error();
};

#endif // DATABASEINTERFACE_H
