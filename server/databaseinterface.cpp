#include <QSqlError>
#include "databaseinterface.h"
#include "../SpynetCommonFiles/serverinterface.h"

/* Constructor */
DatabaseInterface::DatabaseInterface()
{
	db = QSqlDatabase::addDatabase("QMYSQL");
	db.setHostName("localhost");
	db.setDatabaseName("ECE4574_Proj1_Group1");
}

/* Summary:
 *  Error tool helps report errors.
 */
void DatabaseInterface::error()
{
    qDebug() << "Error: " << db.lastError();
}

/* Summary:
 *  Registers a user by adding them to the users table. This can only be
 *  called by the manager.
 * 
 * username [QString*]:
 *  Plain-text username to add.
 * 
 * password [QString*]"
 *  Plain-text password of the username to add.
 */
bool DatabaseInterface::registerUser(const QString* username, const QString* password)
{
    QSqlQuery createUserQ(db);
    QSqlQuery grantSelectQ(db);
    QSqlQuery grantInsertQ(db);
    QSqlQuery insertShadowQ(db);

    // Binding values won't work here
    createUserQ.prepare("CREATE USER '" % *username % "'@'localhost' IDENTIFIED BY '"
            % *password % "';");
    grantSelectQ.prepare("GRANT SELECT ON * TO " % *username % "@'localhost';");
    grantInsertQ.prepare("GRANT INSERT ON Logs TO " % *username % "@'localhost';");
    insertShadowQ.prepare("INSERT INTO `Users` (`name`) VALUES ('" % *username % "');");

    bool ok = createUserQ.exec() && grantSelectQ.exec() && grantInsertQ.exec() 
        && insertShadowQ.exec();
    
    if (!ok)
    {
        error();
    }

    return ok;
}

/* Summary:
 *  Changes the password for an existing user.
 *
 * Returns [bool]:
 *  TRUE if changing password was successful, FALSE otherwise
 *
 * newPassword [QString*]:
 *  The current password in an plain text format(must be valid).
 */
bool DatabaseInterface::changePassword(const QString* newPassword)
{
    QSqlQuery query(db);

    qDebug() << "In change password: " << *newPassword;

    // Have to bind the values a little differently here for some reason?
    query.prepare("SET PASSWORD = Password(?);");
    query.addBindValue(*newPassword);

    bool ok = query.exec();

    if (!ok)
    {
        error();
    }

    return query.exec();
}

/* Summary:
 *  Validates a given user against the database to see if entry should be granted.
 *
 * Returns [bool]:
 *  TRUE if the user is indeed valid, FALSE otherwise.
 *
 * username [QString*]:
 *  The plain-text username we are attempting to validate.
 *
 * encryptedPassword [QString*]:
 *  The encrypted-text password we are verifying for the given user.
 */
bool DatabaseInterface::validateUser(const QString* username, const QString* password)
{
    qDebug() << "In validate user. username: " << *username << " password: " << *password ;

	db.setUserName(*username);
	db.setPassword(*password);

	bool ok = db.open();

    if (!ok)
    {
        error();
    }

    return ok;
}

/* Summary:
 *  Adds a new log to the log tables and associates it with the given username.
 *  The timestamp for the log must also be generated here.
 *
 * Returns [bool]:
 *  TRUE if adding the log was successful, FALSE otherwise.
 *
 * username [QString*]:
 *  The username for which to associate the new log with (this must be the 
 *  same as the logged-in username otherwise we have a security hole)
 *
 * log [QString*]:
 *  The plain-text log data text to add to the table.
 */
bool DatabaseInterface::addNewLog(const QString* username, const QString* log)
{
    bool ok;
    QSqlQuery addLogQ(db);

    addLogQ.prepare("INSERT INTO `Logs` (`id`, `time_posted`, `poster_id`,"
            "`text`) VALUES (NULL, NULL, :username, :log);");
    addLogQ.bindValue(":log", *log);
    addLogQ.bindValue(":username", *username);

    qDebug() << "Add new log. User: " << *username << " log: " << *log;

    ok = addLogQ.exec();

    if (!ok)
    {
        error();
    }

    return ok;
}

/* Summary:
 *  Retrieve the logs for a given username. Pay respect to permissions of the
 *  user.
 *
 * Returns [QString*]:
 *  All the logs that the user can see, in a format ready to populate
 *  the text area in the skynet application.
 *
 * username [QString*]:
 *  The username for which to receive logs for.
 */
QString DatabaseInterface::retrieveLogs(const QString* username)
{
    QSqlQuery logsQ(db);
    QString ret;
    bool ok;
    logsQ.prepare("SELECT poster_id, text, time_posted FROM Logs WHERE poster_id IN ("
        "SELECT user_id FROM Groups_members WHERE group_id IN ("
        "SELECT group_id FROM Users_permissions WHERE user_id='" + *username + "')) "
        "OR poster_id=:username;");
    logsQ.bindValue(":username", *username);

    ok = logsQ.exec();

    if (!ok)
    {
        error();
    }

    logsQ.first();

    QStringList logs;
    do {
        QString log = "";
        for (int fieldNum = 0; fieldNum < 3; fieldNum++)
        {
            log += logsQ.value(fieldNum).toString();  

            if (fieldNum != 2)
            {
                log += RESP_INFO_DELIMITER;
            }
        }
        
        logs << log;
    }
    while (logsQ.next());

    ret = logs.join(RESP_DELIMITER);

    qDebug() << "retrieveLogs for " << *username << ": " << ret;

    return ret;
}

/* Summary:
 *  Get the groups that a user is in, and the groups that the user has permissions
 *  to view.
 *
 * Returns [QString]:
 *  Return a string with information arranged like so:
 *     <username> RESP_INFO_DELIMITER <user's groups> RESP_INFO_DELIMITER <user's permissions> RESP_DELIMITER 
 *     <username> RESP_INFO_DELIMITER <user's groups> RESP_INFO_DELIMITER <user's permissions> RESP_DELIMITER 
 *     ...
 */
QString DatabaseInterface::getAllGroupData()
{
    QSqlQuery usersQ(db);
    QSqlQuery groupDataQ(db);
    QSqlQuery permissionsDataQ(db);
    QString ret = "";
    bool groupDataQOk;
    bool permissionsDataQOk;
    bool usersQOk;
    usersQ.prepare("SELECT name FROM Users;");
    permissionsDataQ.prepare("SELECT user_id, group_id FROM Users_permissions;");
    groupDataQ.prepare("SELECT user_id, group_id FROM Groups_members;");

    usersQOk = usersQ.exec();
    
    if (!usersQOk)
    {
        error();
    }

    usersQ.first();

    QStringList users;
    do 
    {
        users << usersQ.value(0).toString();
    }
    while (usersQ.next());

    groupDataQOk = groupDataQ.exec();

    if (!groupDataQOk)
    {
        error();
    }

    groupDataQ.first();

    // The groups that each member is a part of
    QMap<QString, QString> usersGroups;
    do {
        QString user = groupDataQ.value(0).toString();
        if (!usersGroups.contains(user))
        {
            usersGroups[user] = groupDataQ.value(1).toString();
        }
        else 
        {
            // the user already attached to at least one group, so append...
            usersGroups[user] = usersGroups[user] % ", "
                % groupDataQ.value(1).toString();
        }
    } 
    while (groupDataQ.next());

    permissionsDataQOk = permissionsDataQ.exec();

    if (!permissionsDataQOk)
    {
        error();
    }

    permissionsDataQ.first();

    // The groups that each member has access to
    QMap<QString, QString> usersPermissions;
    do {
        QString user = permissionsDataQ.value(0).toString();
        if (!usersPermissions.contains(user))
        {
            usersPermissions[user] = permissionsDataQ.value(1).toString();
        }
        else 
        {
            // the user already attached to at least one group, so append...
            usersPermissions[user] = usersPermissions[user] % ", "
                % permissionsDataQ.value(1).toString();
        }
    } 
    while (permissionsDataQ.next());

    QStringListIterator userIterator(users);
    while (userIterator.hasNext()) {
        QString user = userIterator.next();
        ret += user % RESP_INFO_DELIMITER % usersGroups[user] % 
            RESP_INFO_DELIMITER % usersPermissions[user];
        if (userIterator.hasNext())
        {
            ret += RESP_DELIMITER;
        }
    }

    qDebug() << "get all group data " << ret;

    return ret;
}

/* Summary:
 *  This is a managerial call to the database to update permissions
 *  according to the new request from the manager.
 *
 * Returns [bool]:
 *  TRUE if updating the permissions was successful, FALSE otherwise
 */
bool DatabaseInterface::changePermissionsGroups(const QString* username, const QString* newGroups)
{
    QSqlQuery addPermissionsQ(db);
    QSqlQuery removePermissionsQ(db);
    QString groupsList = (*newGroups);
    bool removePermissionsQOk;
    bool addPermissionsQOk = true;
    removePermissionsQ.prepare("DELETE FROM Users_permissions WHERE user_id=:username;");
    removePermissionsQ.bindValue(":username", *username);

    removePermissionsQOk = removePermissionsQ.exec();

    if (!removePermissionsQOk)
    {
        error();
    }
    groupsList.remove(" ");
    QStringList groups = groupsList.split(",");

    QStringListIterator groupIterator(groups);
    while (groupIterator.hasNext())
    {
        QString group = groupIterator.next();

        if (group != "")
        {
            continue;
        }

        addPermissionsQ.prepare("INSERT INTO Users_permissions (id, user_id, group_id)"
            " VALUES (NULL, :username, :groupname);");
        addPermissionsQ.bindValue(":username", *username);
        addPermissionsQ.bindValue(":groupname", group);
        qDebug() << "group " << group;

        addPermissionsQOk &= addPermissionsQ.exec();
    }

    if (!addPermissionsQOk)
    {
        error();
    }

    qDebug() << "change permissions groups for " << *username << ": " << *newGroups;

    return removePermissionsQOk && addPermissionsQOk;
}

bool DatabaseInterface::changeMemberGroups(const QString* username, const QString* newGroups)
{
    QSqlQuery addMembersQ(db);
    QSqlQuery removeMembersQ(db);
    QString groupsList = (*newGroups);
    bool removeMembersQOk;
    bool addMembersQOk = true;
    removeMembersQ.prepare("DELETE FROM Groups_members WHERE user_id=:username;");
    removeMembersQ.bindValue(":username", *username);

    removeMembersQOk = removeMembersQ.exec();

    if (!removeMembersQOk)
    {
        error();
    }
    groupsList.remove(" ");
    QStringList groups = groupsList.split(",");

    QStringListIterator groupIterator(groups);
    while (groupIterator.hasNext())
    {
        QString group = groupIterator.next();

        if (group == "")
        {
            continue;
        }

        addMembersQ.prepare("INSERT INTO Groups_members (id, group_id, user_id)"
            " VALUES (NULL, :groupname, :username);");
        addMembersQ.bindValue(":groupname", group);
        addMembersQ.bindValue(":username", *username);
        qDebug() << "group " << group;

        addMembersQOk &= addMembersQ.exec();
    }

    if (!addMembersQOk)
    {
        error();
    }

    qDebug() << "change permissions groups for " << *username << ": " << *newGroups;

    return removeMembersQOk && addMembersQOk;
}
