#include <QtCore>
#include <QtNetwork>
#include <stdlib.h>
#include <iostream>
#include "server.h"
#include "sslserver.h"
#include "../SpynetCommonFiles/serverinterface.h"
using namespace std;

Server::Server()
{

}

void Server::run(qint16 port)
{
    QString ipAddress;

    sslServer = new SSLServer();
    if (!sslServer->listen(QHostAddress::Any, port)) 
    {
        qDebug() << tr("Unable to start the server.\n%1")
            .arg(sslServer->errorString()) << endl;
        return;
    }

    QList<QHostAddress> ipAddressList = QNetworkInterface::allAddresses();
    for (int i = 0; i < ipAddressList.size(); i++)
    {
        if (ipAddressList.at(i) != QHostAddress::LocalHost &&
                ipAddressList.at(i).toIPv4Address()) {
            ipAddress = ipAddressList.at(i).toString();
            break;
        }
    }
    
    // If we did not find an ip address, resort to a local address
    if (ipAddress.isEmpty())
    {
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    }

    port = sslServer->serverPort();

    qDebug() << tr("Starting server on %1:%2")
        .arg(ipAddress)
        .arg(port);

    connect(sslServer, SIGNAL(newConnection()), this, SLOT(openConnection()));

    // Initialize connection to the db
    databaseInterface = new DatabaseInterface();
}

void Server::openConnection()
{
    QSslSocket *clientConnection = sslServer->nextPendingConnection();
    if (!clientConnection->waitForEncrypted(1000)) {
        qDebug() << "Waited 1 second for encryption handshake without success";
        return;
    }

    qDebug() << "Succesfully waited for secure handshake.";

    connect(clientConnection, SIGNAL(readyRead()), this, SLOT(handleRequest()));
    connect(clientConnection, SIGNAL(disconnected()),
            clientConnection, SLOT(deleteLater()));

}

/* Summary:
 *  Handle incoming requests from the client. First read the request, then
 *  parse out the request method from the arguments. We then call
 *  the corresponding database method, and write the result back to the client
 *  when ready.
 */
void Server::handleRequest()
{
    QSslSocket* clientConnection = (QSslSocket*)(QObject::sender());

    QString request = clientConnection->readLine().data();
    qDebug() << "Read request: " << request.remove('\n');

    QStringList requestParts = request.split(REQUEST_DELIMITER); 

    // The method is always the first argument
    QString method = requestParts.at(0);
    qDebug() << "Method: " << method;

    QString response;
    if (method == "VALIDATE_USER")
    {
        const QString username = requestParts.at(1);
        const QString password = requestParts.at(2);
        response = boolToString(databaseInterface->validateUser(&username, 
                        &password));
    }
    else if (method == "REGISTER_USER")
    {
        const QString username = requestParts.at(1);
        const QString password = requestParts.at(2);
        response = boolToString(databaseInterface->registerUser(&username,
                    &password));
    }
    else if (method == "CHANGE_PASSWORD")
    {
        const QString newPassword = requestParts.at(1);
        response = boolToString(databaseInterface->changePassword(&newPassword));
    }
    else if (method == "RETRIEVE_LOGS")
    {
        // This should only be possible to do with the logged-in user
        const QString username = requestParts.at(1);
        response = databaseInterface->retrieveLogs(&username);
    }
    else if (method == "ADD_LOG")
    {
        const QString username = requestParts.at(1);
        const QString log = requestParts.at(2);
        response = boolToString(databaseInterface->addNewLog(&username,
                &log));
    }
    else if (method == "CHANGE_PERMISSIONS_GROUPS")
    {
        const QString username = requestParts.at(1);
        const QString newPermissions = requestParts.at(2);
		response = boolToString(databaseInterface->changePermissionsGroups(&username, 
                &newPermissions));
    }
    else if (method == "CHANGE_MEMBER_GROUPS")
    {
        const QString username = requestParts.at(1);
        const QString memberGroups = requestParts.at(2);
        response = boolToString(databaseInterface->changeMemberGroups(&username, &memberGroups));
    }
    else if (method == "GET_ALL_GROUP_DATA")
    {
        // Close the connection when we receive the logout signal
        response = databaseInterface->getAllGroupData();
    }
    else if (method == "LOGOUT")
    {
        // Close the connection when we receive the logout signal
        clientConnection->disconnectFromHost();
        return;
    }

    qDebug() << "Response server will send: " << response;

    // Now we write back
    clientConnection->write(response.toLocal8Bit().data());

    qDebug() << "Successfully sent response.";
}

QString Server::boolToString(bool b)
{
    QString ret;
    if (b)
    {
        ret = "true";
    }
    else 
    {
        ret = "false";
    }
    return ret;
}
