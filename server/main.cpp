
#include <QCoreApplication>
#include <QtCore>
#include <stdlib.h>
#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    Server server;

    qint16 port;

    if (argc < 2)
    {
        qDebug() << "Using automatic port number";
        port= 5000;
    }
    else
    {
        port= atoi(argv[1]);
    }

    server.run(port);

    return app.exec();
}
