#ifndef SERVER_H
#define SERVER_H
#include <QObject>
#include "sslserver.h"
#include "databaseinterface.h"

QT_BEGIN_NAMESPACE
class QTcpServer;
QT_END_NAMESPACE;

class Server : public QObject
{
    Q_OBJECT

public:
    Server();
    void run(qint16 port);

private slots:
    void openConnection();
    void handleRequest();

private:
    SSLServer *sslServer;
    DatabaseInterface *databaseInterface;

    QString boolToString(bool);

};

#endif


