#ifndef SERVERINTERFACE_H
#define SERVERINTERFACE_H

#include <QTcpSocket>
#include <QSslError>
#include <QSslCertificate>
#include <QSslKey>

QT_BEGIN_NAMESPACE
class QSslSocket;
class QSslCertificate;
QT_END_NAMESPACE;

enum RequestType {
    VALIDATE_USER,
    REGISTER_USER,
    CHANGE_PASSWORD,
    RETRIEVE_LOGS,
    ADD_LOG,
    CHANGE_PERMISSIONS_GROUPS,
    CHANGE_MEMBER_GROUPS,
    GET_ALL_GROUP_DATA,
    LOGOUT
};

const QString REQUEST_DELIMITER = "^$*#(%&*^%^(%^#*(%)^&*!";
const QString RESP_DELIMITER = "|||";
const QString RESP_INFO_DELIMITER = "###";

class ServerInterface: public QObject
{
    Q_OBJECT

public:
    ServerInterface();
    bool connectToHost(const QString host, qint16 port);
    void callServer(RequestType type,
            QStringList args);

    RequestType getCurrentRequestType() {return m_currentRequestType;}

private slots:
    void readResponse();
    void handleSSLError(QList<QSslError>);
    void displayError(QAbstractSocket::SocketError socketError);

signals:
    void serverResponse(QString* response);

private:
    QSslSocket *secureSocket;
    quint16 blockSize;

    QString requestToString(RequestType type);
    RequestType m_currentRequestType;
};

#endif // SERVERINTERFACE_H
