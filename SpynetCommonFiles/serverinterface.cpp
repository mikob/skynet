#include <QtCore>
#include <QtNetwork>
#include <QDebug>
#include <QStringBuilder>
#include "serverinterface.h"

ServerInterface::ServerInterface()
{
}

bool ServerInterface::connectToHost(const QString host, qint16 port)
{
    qDebug() << tr("Attempting connection to %1:%2")
            .arg(host)
            .arg(port);

    // Get the ip address to connect to
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

    secureSocket = new QSslSocket(this);

    // special slot to handle errors with the certificates
    // in particular the fact that they are self-signed
    connect(secureSocket, SIGNAL(sslErrors(QList<QSslError>)), this,
            SLOT(handleSSLError(QList<QSslError>)));
    connect(secureSocket, SIGNAL(readyRead()), this, SLOT(readResponse()));
    connect(secureSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));

    // Start the connection here?
    blockSize = 0;
    secureSocket->abort();
    secureSocket->setPeerVerifyMode(QSslSocket::QueryPeer);
    secureSocket->connectToHostEncrypted(host, port);

    if (!secureSocket->waitForEncrypted(1000)) {
        qDebug() << "Error: Could not connect to host";
        return false;
    }

    return true;
}

void ServerInterface::callServer(RequestType type,
        QStringList args)
{
    m_currentRequestType = type;
    QString fullRequest = requestToString(type) % REQUEST_DELIMITER % args.join(REQUEST_DELIMITER) + '\n';
    QString debugOut = fullRequest;

    qDebug() << "fullRequest: " << debugOut.remove('\n');

    secureSocket->write(fullRequest.toLocal8Bit().data());
}

void ServerInterface::readResponse()
{
    qDebug() << "In server response";

    qDebug() << "bytes available" << secureSocket->bytesAvailable();
    QString response = secureSocket->readAll().data();

    emit ServerInterface::serverResponse(&response);
}

void ServerInterface::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
            break;
        case QAbstractSocket::HostNotFoundError:
            qDebug() << "Host not found";
            break;
        case QAbstractSocket::ConnectionRefusedError:
            qDebug() << "Connection refused";
            break;
        default:
            qDebug() << "Error: " << secureSocket->errorString();
    }
}

void ServerInterface::handleSSLError(QList<QSslError> errorList)
{
    foreach (QSslError error, errorList) {
        qDebug() << "SSL Error: " << error;
    }

    secureSocket->ignoreSslErrors();
}

QString ServerInterface::requestToString(RequestType type)
{
    QString ret;
    switch (type) {
        case VALIDATE_USER:
            ret = "VALIDATE_USER";
            break;
        case REGISTER_USER:
            ret = "REGISTER_USER";
            break;
        case CHANGE_PASSWORD:
            ret = "CHANGE_PASSWORD";
            break;
        case RETRIEVE_LOGS:
            ret = "RETRIEVE_LOGS";
            break;
        case ADD_LOG:
            ret = "ADD_LOG";
            break;
        case CHANGE_PERMISSIONS_GROUPS:
            ret = "CHANGE_PERMISSIONS_GROUPS";
            break;
        case CHANGE_MEMBER_GROUPS:
            ret = "CHANGE_MEMBER_GROUPS";
            break;
        case GET_ALL_GROUP_DATA:
            ret = "GET_ALL_GROUP_DATA";
            break;
        case LOGOUT:
            ret = "LOGOUT";
            break;
    }

    return ret;
}
