#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStandardItemModel>
#include <QSplitter>
#include <QDebug>
#include <QInputDialog>
#include <iostream>
#include <QList>
#include <QString>
#include <QRegExp>
#include <QMessageBox>
#include <time.h>;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);
    srand(time(NULL));

    ui->tableWidget->horizontalHeader()->resizeSection(0,140);
    ui->tableWidget->horizontalHeader()->resizeSection(1,301);
    ui->tableWidget->horizontalHeader()->resizeSection(2,301);
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(3);

    connect(ui->btnLogin,SIGNAL(clicked()),this,SLOT(btnLoginClicked()));
    connect(ui->btnAddUser,SIGNAL(clicked()),this,SLOT(btnAddUserClicked()));
    connect(ui->mnuLogout,SIGNAL(triggered()),this,SLOT(mnuLogoutClicked()));
    connect(ui->mnuExit,SIGNAL(triggered()),this,SLOT(close()));
    connect(ui->tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(editCell(int,int)));
    connect(&skynetServer, SIGNAL(serverResponse(QString*)), this, SLOT(handleResponse(QString*)));

    // defaults
//    ui->leHost->setText("10.0.2.15");
//    ui->lePort->setText("5000");
//    ui->lePassword->setText("Hunter2");
}

void MainWindow::handleResponse(QString* response)
{
    QString res = *response;

    switch (skynetServer.getCurrentRequestType()) {
        case VALIDATE_USER:
            if (res != "true")
            {
                ui->lblLoginError->setText("Error: Invalid password");
                skynetServer.callServer(LOGOUT, QStringList() << "");
                return;
            }

            ui->stackedWidget->setCurrentIndex(1);

            if (ui->tableWidget->rowCount() == 0)
                skynetServer.callServer(GET_ALL_GROUP_DATA, QStringList() << "");
            return;
        case GET_ALL_GROUP_DATA:
        {
            //QList<QString> tblData;
            QStringList splitTableData = res.split(RESP_DELIMITER);
            QStringList splitEntry;
            foreach (QString tableEntry, splitTableData)
            {
                splitEntry = tableEntry.split(RESP_INFO_DELIMITER);
                MainWindow::m_usernames << splitEntry.at(0);

                ui->tableWidget->insertRow(ui->tableWidget->rowCount());
                for  (int  col=0;col<ui->tableWidget->columnCount();  col++)
                {
                    if (col == 1) //removing username from member groups before displaying
                    {
                        QString memberGroups = splitEntry.at(col);
                        int posOfUserName = memberGroups.lastIndexOf(',');
                        if (posOfUserName != -1)
                        {
                            memberGroups.truncate(posOfUserName);
                            splitEntry[col] = memberGroups;
                        }
                    }

                    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1,  col, new  QTableWidgetItem(splitEntry.at(col)));
                }
            }

            for (int row=0;row<ui->tableWidget->rowCount();row++)
            {
                for  (int  col=0;col<ui->tableWidget->columnCount();  col++)
                {
                    ui->tableWidget->item(row,col)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
                }
            }

            ui->tableWidget->sortByColumn(0,Qt::AscendingOrder);

            return;
        }
        case REGISTER_USER:
        {
            QString username = ui->tableWidget->item(ui->tableWidget->rowCount() - 1, 0)->text();
            skynetServer.callServer(CHANGE_MEMBER_GROUPS,QStringList() << username << username);
            ui->tableWidget->sortByColumn(0,Qt::AscendingOrder);
            return;
        }
    }
}

void MainWindow::editCell(int row, int col)
{
    if (col == 0) return;

    QString oldText = ui->tableWidget->item(row,col)->text();
    QString username = ui->tableWidget->item(row,0)->text();
    QString title = col == 1 ? "Set " + username + "'s Members" : "Set " + username + "'s Permissions";

    bool ok;
    QString newText = QInputDialog::getText(this, title,"Enter new groups separated by commas\n\n" + username + "'s current groups: " + oldText, QLineEdit::Normal, oldText, &ok);

    if (!ok)  return;

    while (1)
    {
        if (newText.contains("\"") || newText.contains("\\"))
        {
            newText = QInputDialog::getText(this, title,"Error: Groups cannot contain the following characters: \\,\"\n\n" + username + "'s current groups: " + oldText, QLineEdit::Normal, newText, &ok);
        }
        else break;

        if (!ok)  return;
    }

    QStringList newGroups = newText.split(",");
    newText = "";
    foreach (QString group, newGroups)
    {
        group = group.trimmed();
        if (group == "") continue;
        newText.append(group).append(",");
    }
    newText.chop(1);

    if (newText != oldText)
    {
        ui->tableWidget->item(row,col)->setText(newText);

        if (col == 1)
        {
            skynetServer.callServer(CHANGE_MEMBER_GROUPS,QStringList() << username << newText);
        }
        else if (col == 2)
        {
            skynetServer.callServer(CHANGE_PERMISSIONS_GROUPS,QStringList() << username << newText);
        }
    }
}

void MainWindow::mnuLogoutClicked()
{
    skynetServer.callServer(LOGOUT, QStringList() << "");
    ui->lblLoginError->setText("");
    ui->lePassword->setText("");
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::btnAddUserClicked()
{
    bool ok;
    QString username = QInputDialog::getText(this, "Add New User","New username (between 6-20 characters):", QLineEdit::Normal, "", &ok);

    if (!ok)  return;

    while (1)
    {
        if (username.length() < 6 || username.length() > 20)
        {
            username = QInputDialog::getText(this, "Add New User","Error: Username must have at least 6 characters\nand no more than 20 characters", QLineEdit::Normal, "", &ok);
        }
        else if (m_usernames.contains(username))
        {
            username = QInputDialog::getText(this, "Add New User", "Error: " + username + " already exists in the system.", QLineEdit::Normal, "", &ok);
        }
        else break;

        if (!ok)  return;
    }

    int lastRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(lastRow);
    ui->tableWidget->setItem(lastRow, 0, new  QTableWidgetItem(username));
    ui->tableWidget->setItem(lastRow, 1, new  QTableWidgetItem(""));
    ui->tableWidget->setItem(lastRow, 2, new  QTableWidgetItem(""));
    for  (int  col=0;col<ui->tableWidget->columnCount();  col++)
    {
        ui->tableWidget->item(lastRow,col)->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    }

    QString randPassword = QString::number(int(random() / (RAND_MAX + 1.0) * (99999 + 1 - 10000) + 10000));
    QMessageBox::information(this,"New User Password",username + ", please write down your temporary password: " + randPassword);

    skynetServer.callServer(REGISTER_USER, QStringList() << username << randPassword);

    m_usernames << username;
}

void MainWindow::btnLoginClicked()
{
   QString hostname = ui->leHost->text();
   QString port = ui->lePort->text();
   QString username = "manager1";
   QString password = ui->lePassword->text();

    if (hostname == "")
    {
        ui->lblLoginError->setText("Error: Host field empty");
        return;
    }
    else if (port == "")
    {
        ui->lblLoginError->setText("Error: Port field empty");
        return;
    }
    else if (password == "")
    {
        ui->lblLoginError->setText("Error: Password field empty");
        return;
    }
    else if (!skynetServer.connectToHost(hostname, port.toInt()))
    {
        ui->lblLoginError->setText("Error: Could not connect to host.");
        return;
    }

    skynetServer.callServer(VALIDATE_USER, QStringList() << username << password);
}

MainWindow::~MainWindow()
{
    delete ui;
}
