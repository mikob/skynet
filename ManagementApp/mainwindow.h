#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include "../SpynetCommonFiles/serverinterface.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QStringList m_usernames;

    ServerInterface skynetServer;

private slots:
    void btnLoginClicked();
    void btnAddUserClicked();
    void mnuLogoutClicked();
    void editCell(int row, int col);

    void handleResponse(QString* response);

};

#endif // MAINWINDOW_H
