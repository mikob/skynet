#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <stdlib.h>
#include <QStringList>
#include <QFile>
#include <QMessageBox>
#include <QRegExp>
#include <QKeyEvent>


QString g_newPassword;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

//    QFile keyFile("sharedkey.pem");
//    keyFile.open(QIODevice::ReadOnly | QIODevice::Text); //already checked that the file exists in main
//    m_crypt.setKey(keyFile.readLine().toInt());

    ui->stackedWidget->setCurrentIndex(0);

    connect(ui->btnLogin,SIGNAL(clicked()),this,SLOT(btnLoginClicked()));
    connect(ui->mnuPassword,SIGNAL(triggered()),this,SLOT(mnuPasswordClicked()));
    connect(ui->mnuLogout,SIGNAL(triggered()),this,SLOT(mnuLogoutClicked()));
    connect(ui->mnuExit,SIGNAL(triggered()),this,SLOT(close()));
    connect(ui->btnChangePassword,SIGNAL(clicked()),this,SLOT(btnChangePasswordClicked()));
    connect(&skynetServer, SIGNAL(serverResponse(QString*)), this, SLOT(handleResponse(QString*)));
    connect(ui->btnRefresh,SIGNAL(clicked()),this,SLOT(btnRefreshClicked()));
    connect(ui->btnCancelPWChange,SIGNAL(clicked()),this,SLOT(btnCancelPWChangeClicked()));

    connect(ui->leHost,SIGNAL(returnPressed()),this,SLOT(btnLoginClicked()));
    connect(ui->leUsername,SIGNAL(returnPressed()),this,SLOT(btnLoginClicked()));
    connect(ui->lePassword,SIGNAL(returnPressed()),this,SLOT(btnLoginClicked()));
    connect(ui->lePort,SIGNAL(returnPressed()),this,SLOT(btnLoginClicked()));
    connect(ui->leNewPassword,SIGNAL(returnPressed()),this,SLOT(btnChangePasswordClicked()));
    connect(ui->leOldPassword,SIGNAL(returnPressed()),this,SLOT(btnChangePasswordClicked()));
    connect(ui->leRetypeNewPassword,SIGNAL(returnPressed()),this,SLOT(btnChangePasswordClicked()));

    ui->txtInput->installEventFilter(this);

    // Setup defaults
//    ui->leHost->setText("10.0.2.15");
//    ui->lePort->setText("5000");
//    ui->leUsername->setText("supervisor1");
//    ui->lePassword->setText("Hunter2");
}

void MainWindow::handleResponse(QString* response)
{
    QString res = *response;

    switch (skynetServer.getCurrentRequestType()) {
        case VALIDATE_USER:
            if (ui->stackedWidget->currentIndex() == 2)
            {
                if (res != "true")
                {
                    ui->lblPasswordError->setText("Error: Old password is incorrect.");
                }
                else
                {
                    skynetServer.callServer(CHANGE_PASSWORD, QStringList() << m_spyname << g_newPassword);
                }

                return;
            }

            if (res != "true")
            {
                ui->lblLoginError->setText("Error: Invalid Username or Password");
                skynetServer.callServer(LOGOUT, QStringList() << "");
                return;
            }

            if (ui->lePassword->text().length() < 6)
            {
                QMessageBox::information(this,"New User Detected",m_spyname + ", please change your temporary password");
                ui->lblPasswordError->setText("");
                ui->stackedWidget->setCurrentIndex(2);
            }
            else
            {
                ui->stackedWidget->setCurrentIndex(1);
            }

            skynetServer.callServer(RETRIEVE_LOGS, QStringList() << m_spyname);
            return;
        case CHANGE_PASSWORD:
            if (res != "true")
            {
                QMessageBox::warning(this,"Password Change","Warning: Password was not changed due to unknown reasons.");
            }
            else
            {
                QMessageBox::information(this,"Password Change","Password Successfully Changed!");
            }

            ui->stackedWidget->setCurrentIndex(1);

            return;
        case RETRIEVE_LOGS:
        {
            m_allLogs.clear();
            ui->txtLogs->clear();

            QStringList splitLogEntries = res.split(RESP_DELIMITER);
            QStringList splitEntry;
            userLog newLog;
            foreach(QString logEntry, splitLogEntries)
            {
                splitEntry = logEntry.split(RESP_INFO_DELIMITER);
                if (splitEntry.at(0).trimmed() != "")
                {
                    newLog.username = splitEntry.at(0);
                    newLog.log = splitEntry.at(1);
                    m_allLogs.insert(QDateTime::fromString(splitEntry.at(2), "yyyy-MM-ddTHH:mm:ss"), newLog);
                }
            }

            updateUserListAndLogs();

            return;
        }
        case LOGOUT:

            break;
    }
}

void MainWindow::btnCancelPWChangeClicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::btnRefreshClicked()
{
    skynetServer.callServer(RETRIEVE_LOGS, QStringList() << m_spyname);
}

void MainWindow::updateUserListAndLogs()
{
    m_checkedUsers.clear();
    for (int row = 0; row < ui->lstUsers->count(); row++)
    {
        if (ui->lstUsers->item(row)->checkState() == Qt::Checked)
        {
            if (ui->lstUsers->item(row)->text() != "")
                m_checkedUsers << ui->lstUsers->item(row)->text();
        }
    }

    ui->lstUsers->clear();

    QStringList allUsers;
    foreach (userLog logItem, m_allLogs)
    {
        allUsers << logItem.username;
    }

    if (m_spyname.trimmed() != "") allUsers << m_spyname;
    allUsers.removeDuplicates();

    foreach (QString user, allUsers)
    {
        ui->lstUsers->addItem(user);
    }

    for (int row = 0; row < ui->lstUsers->count(); row++)
    {
        ui->lstUsers->item(row)->setCheckState(Qt::Unchecked);
        ui->lstUsers->item(row)->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);

        if (m_checkedUsers.contains(ui->lstUsers->item(row)->text()))
        {
            ui->lstUsers->item(row)->setCheckState(Qt::Checked);
        }
    }

    if (m_checkedUsers.empty())
    {
        m_checkedUsers << m_spyname;
        QList<QListWidgetItem *> findSpyInUserList = ui->lstUsers->findItems(m_spyname,Qt::MatchFixedString);
        findSpyInUserList.at(0)->setCheckState(Qt::Checked);
    }

    ui->lstUsers->sortItems();
    ui->lstUsers->setSortingEnabled(true);


    QMapIterator<QDateTime, userLog> i(m_allLogs);
    while (i.hasNext())
    {
        i.next();
        if (m_checkedUsers.contains(i.value().username))
        {
            outputLog(i.value().username,i.key(),i.value().log);
        }
    }
}

void MainWindow::btnLoginClicked()
{
    QString hostname = ui->leHost->text();
    QString port = ui->lePort->text();
    QString username = ui->leUsername->text();
    QString password = ui->lePassword->text();

    if (hostname == "")
    {
        ui->lblLoginError->setText("Error: Host field empty");
        return;
    }
    else if (port == "")
    {
        ui->lblLoginError->setText("Error: Port field empty");
        return;
    }
    else if (username == "")
    {
        ui->lblLoginError->setText("Error: Username field empty");
        return;
    }
    else if (password == "")
    {
        ui->lblLoginError->setText("Error: Password field empty");
        return;
    }
    else if (!skynetServer.connectToHost(hostname, port.toInt()))
    {
        ui->lblLoginError->setText("Error: Could not connect to host.");
        return;
    }

    m_spyname = username;
    //skynetServer.callServer(VALIDATE_USER, QStringList() << username << m_crypt.encryptToString(password));
    skynetServer.callServer(VALIDATE_USER, QStringList() << username << password);
}

void MainWindow::mnuPasswordClicked()
{
    if (ui->stackedWidget->currentIndex() == 2) return;

    if (ui->stackedWidget->currentIndex() == 0)
    {
        QMessageBox::warning(this,"Error","You must login before changing your password");
        return;
    }

    ui->leOldPassword->setText("");
    ui->leNewPassword->setText("");
    ui->leRetypeNewPassword->setText("");
    ui->lblPasswordError->setText("");
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::btnChangePasswordClicked()
{
    QString oldPassword = ui->leOldPassword->text();
    QString newPassword = ui->leNewPassword->text();
    QString retypeNewPassword = ui->leRetypeNewPassword->text();

    if (oldPassword == "")
    {
        ui->lblPasswordError->setText("Error: Old Password field empty");
        return;
    }
    else if (newPassword == "")
    {
        ui->lblPasswordError->setText("Error: New Password field empty");
        return;
    }
    else if (newPassword != retypeNewPassword)
    {
        ui->lblPasswordError->setText("Error: Both new password fields must match.");
        return;
    }
    else if (newPassword.length() < 6 || newPassword.contains("\"") || !newPassword.contains(QRegExp("^\\w*(?=\\w*\\d)(?=\\w*[a-z])(?=\\w*[A-Z])\\w*$")) || newPassword.contains("\\"))
    {
        ui->lblPasswordError->setText("Error: New password does not meet all requirements listed above");
        return;
    }
    else if (newPassword == m_spyname)
    {
        ui->lblPasswordError->setText("Error: New password may not be the same your username");
        return;
    }

    g_newPassword = newPassword;
    skynetServer.callServer(CHANGE_PASSWORD, QStringList() << newPassword);
}

void MainWindow::outputLog(QString username, QDateTime time, QString log)
{
    QString horizontalRule = ui->txtLogs->toPlainText() == "" ? "" : "<br><hr><br>";
    ui->txtLogs->moveCursor(QTextCursor::End);
    ui->txtLogs->insertHtml(horizontalRule + "<font color=\"blue\"><b>" + username + "</b></font> <font color=\"red\"><i>(" + time.toString("M/d/yy h:mm:ss ap") + ")</i></font>:<br>" + log);
    ui->txtLogs->ensureCursorVisible();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mnuLogoutClicked()
{
    skynetServer.callServer(LOGOUT, QStringList() << "");
    m_allLogs.clear();
    ui->lstUsers->clear();
    ui->stackedWidget->setCurrentIndex(0);
    ui->lblLoginError->setText("");
    ui->txtLogs->clear();
    ui->txtInput->clear();
    ui->leUsername->setText("");
    ui->lePassword->setText("");
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
         QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
         if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return)
         {
             QString plainTextLog = ui->txtInput->toPlainText();
             QDateTime currentTime = QDateTime::currentDateTime();

             if (plainTextLog.trimmed() == "")
                 return true;

             skynetServer.callServer(ADD_LOG, QStringList() << m_spyname << plainTextLog);
             userLog newLog;
             newLog.username = m_spyname;
             newLog.log = plainTextLog;
             m_allLogs.insert(currentTime, newLog);

             if (m_checkedUsers.contains(m_spyname)) outputLog(m_spyname, currentTime, plainTextLog);
             ui->txtInput->clear();

             return true;
         }
     }

    return QObject::eventFilter(obj, event);
}
