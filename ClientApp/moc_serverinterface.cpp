/****************************************************************************
** Meta object code from reading C++ file 'serverinterface.h'
**
** Created: Tue Feb 21 19:36:46 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SpynetCommonFiles/serverinterface.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serverinterface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ServerInterface[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   17,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      51,   16,   16,   16, 0x08,
      66,   16,   16,   16, 0x08,
     111,   99,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ServerInterface[] = {
    "ServerInterface\0\0response\0"
    "serverResponse(QString*)\0readResponse()\0"
    "handleSSLError(QList<QSslError>)\0"
    "socketError\0displayError(QAbstractSocket::SocketError)\0"
};

const QMetaObject ServerInterface::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ServerInterface,
      qt_meta_data_ServerInterface, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ServerInterface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ServerInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ServerInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ServerInterface))
        return static_cast<void*>(const_cast< ServerInterface*>(this));
    return QObject::qt_metacast(_clname);
}

int ServerInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: serverResponse((*reinterpret_cast< QString*(*)>(_a[1]))); break;
        case 1: readResponse(); break;
        case 2: handleSSLError((*reinterpret_cast< QList<QSslError>(*)>(_a[1]))); break;
        case 3: displayError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ServerInterface::serverResponse(QString * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
