/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Feb 22 11:20:08 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *mnuPassword;
    QAction *mnuLogout;
    QAction *mnuExit;
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *loginPage;
    QLineEdit *lePassword;
    QPushButton *btnLogin;
    QLineEdit *leUsername;
    QLabel *label;
    QLabel *label_2;
    QLabel *lblLoginError;
    QLabel *label_3;
    QLineEdit *leHost;
    QLineEdit *lePort;
    QLabel *label_4;
    QWidget *mainPage;
    QTextEdit *txtLogs;
    QTextEdit *txtInput;
    QListWidget *lstUsers;
    QPushButton *btnRefresh;
    QWidget *passwordPage;
    QLineEdit *leOldPassword;
    QLineEdit *leNewPassword;
    QLabel *label_6;
    QLabel *label_7;
    QPushButton *btnChangePassword;
    QLabel *label_5;
    QLabel *lblPasswordError;
    QLabel *label_8;
    QLineEdit *leRetypeNewPassword;
    QPushButton *btnCancelPWChange;
    QMenuBar *menuBar;
    QMenu *menuUser;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(780, 436);
        MainWindow->setMinimumSize(QSize(780, 436));
        MainWindow->setMaximumSize(QSize(780, 436));
        mnuPassword = new QAction(MainWindow);
        mnuPassword->setObjectName(QString::fromUtf8("mnuPassword"));
        mnuLogout = new QAction(MainWindow);
        mnuLogout->setObjectName(QString::fromUtf8("mnuLogout"));
        mnuExit = new QAction(MainWindow);
        mnuExit->setObjectName(QString::fromUtf8("mnuExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 780, 436));
        loginPage = new QWidget();
        loginPage->setObjectName(QString::fromUtf8("loginPage"));
        lePassword = new QLineEdit(loginPage);
        lePassword->setObjectName(QString::fromUtf8("lePassword"));
        lePassword->setGeometry(QRect(370, 180, 113, 20));
        lePassword->setEchoMode(QLineEdit::Password);
        btnLogin = new QPushButton(loginPage);
        btnLogin->setObjectName(QString::fromUtf8("btnLogin"));
        btnLogin->setGeometry(QRect(350, 220, 75, 23));
        leUsername = new QLineEdit(loginPage);
        leUsername->setObjectName(QString::fromUtf8("leUsername"));
        leUsername->setGeometry(QRect(370, 150, 113, 20));
        label = new QLabel(loginPage);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(290, 150, 71, 20));
        label_2 = new QLabel(loginPage);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(290, 180, 71, 20));
        lblLoginError = new QLabel(loginPage);
        lblLoginError->setObjectName(QString::fromUtf8("lblLoginError"));
        lblLoginError->setEnabled(true);
        lblLoginError->setGeometry(QRect(290, 50, 231, 31));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 127, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(255, 63, 63, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(127, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(170, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush7(QColor(255, 255, 220, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lblLoginError->setPalette(palette);
        lblLoginError->setWordWrap(true);
        label_3 = new QLabel(loginPage);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(290, 90, 71, 20));
        leHost = new QLineEdit(loginPage);
        leHost->setObjectName(QString::fromUtf8("leHost"));
        leHost->setGeometry(QRect(370, 90, 113, 20));
        lePort = new QLineEdit(loginPage);
        lePort->setObjectName(QString::fromUtf8("lePort"));
        lePort->setGeometry(QRect(370, 120, 113, 20));
        label_4 = new QLabel(loginPage);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(290, 120, 71, 20));
        stackedWidget->addWidget(loginPage);
        mainPage = new QWidget();
        mainPage->setObjectName(QString::fromUtf8("mainPage"));
        txtLogs = new QTextEdit(mainPage);
        txtLogs->setObjectName(QString::fromUtf8("txtLogs"));
        txtLogs->setGeometry(QRect(10, 10, 641, 301));
        txtLogs->setReadOnly(true);
        txtInput = new QTextEdit(mainPage);
        txtInput->setObjectName(QString::fromUtf8("txtInput"));
        txtInput->setGeometry(QRect(10, 330, 761, 71));
        lstUsers = new QListWidget(mainPage);
        lstUsers->setObjectName(QString::fromUtf8("lstUsers"));
        lstUsers->setGeometry(QRect(660, 10, 111, 261));
        btnRefresh = new QPushButton(mainPage);
        btnRefresh->setObjectName(QString::fromUtf8("btnRefresh"));
        btnRefresh->setGeometry(QRect(660, 280, 111, 27));
        stackedWidget->addWidget(mainPage);
        passwordPage = new QWidget();
        passwordPage->setObjectName(QString::fromUtf8("passwordPage"));
        leOldPassword = new QLineEdit(passwordPage);
        leOldPassword->setObjectName(QString::fromUtf8("leOldPassword"));
        leOldPassword->setGeometry(QRect(370, 190, 113, 20));
        leOldPassword->setEchoMode(QLineEdit::Password);
        leNewPassword = new QLineEdit(passwordPage);
        leNewPassword->setObjectName(QString::fromUtf8("leNewPassword"));
        leNewPassword->setGeometry(QRect(370, 220, 113, 20));
        leNewPassword->setEchoMode(QLineEdit::Password);
        label_6 = new QLabel(passwordPage);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(260, 220, 101, 20));
        label_7 = new QLabel(passwordPage);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(260, 190, 101, 20));
        btnChangePassword = new QPushButton(passwordPage);
        btnChangePassword->setObjectName(QString::fromUtf8("btnChangePassword"));
        btnChangePassword->setGeometry(QRect(350, 280, 131, 21));
        label_5 = new QLabel(passwordPage);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(260, 40, 231, 91));
        label_5->setWordWrap(true);
        lblPasswordError = new QLabel(passwordPage);
        lblPasswordError->setObjectName(QString::fromUtf8("lblPasswordError"));
        lblPasswordError->setGeometry(QRect(260, 150, 231, 31));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        lblPasswordError->setPalette(palette1);
        lblPasswordError->setWordWrap(true);
        label_8 = new QLabel(passwordPage);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(210, 250, 151, 20));
        leRetypeNewPassword = new QLineEdit(passwordPage);
        leRetypeNewPassword->setObjectName(QString::fromUtf8("leRetypeNewPassword"));
        leRetypeNewPassword->setGeometry(QRect(370, 250, 113, 20));
        leRetypeNewPassword->setEchoMode(QLineEdit::Password);
        btnCancelPWChange = new QPushButton(passwordPage);
        btnCancelPWChange->setObjectName(QString::fromUtf8("btnCancelPWChange"));
        btnCancelPWChange->setGeometry(QRect(270, 280, 71, 21));
        stackedWidget->addWidget(passwordPage);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 780, 25));
        menuUser = new QMenu(menuBar);
        menuUser->setObjectName(QString::fromUtf8("menuUser"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(leOldPassword, leNewPassword);
        QWidget::setTabOrder(leNewPassword, leRetypeNewPassword);
        QWidget::setTabOrder(leRetypeNewPassword, btnChangePassword);
        QWidget::setTabOrder(btnChangePassword, leHost);
        QWidget::setTabOrder(leHost, lePort);
        QWidget::setTabOrder(lePort, leUsername);
        QWidget::setTabOrder(leUsername, lePassword);
        QWidget::setTabOrder(lePassword, btnLogin);
        QWidget::setTabOrder(btnLogin, txtLogs);
        QWidget::setTabOrder(txtLogs, txtInput);
        QWidget::setTabOrder(txtInput, btnRefresh);
        QWidget::setTabOrder(btnRefresh, lstUsers);

        menuBar->addAction(menuUser->menuAction());
        menuUser->addAction(mnuPassword);
        menuUser->addAction(mnuLogout);
        menuUser->addAction(mnuExit);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "SpyNet - Client", 0, QApplication::UnicodeUTF8));
        mnuPassword->setText(QApplication::translate("MainWindow", "Change Password", 0, QApplication::UnicodeUTF8));
        mnuLogout->setText(QApplication::translate("MainWindow", "Log out", 0, QApplication::UnicodeUTF8));
        mnuExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        lePassword->setText(QString());
        btnLogin->setText(QApplication::translate("MainWindow", "Login", 0, QApplication::UnicodeUTF8));
        leUsername->setText(QString());
        label->setText(QApplication::translate("MainWindow", "Username", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Password", 0, QApplication::UnicodeUTF8));
        lblLoginError->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "Host", 0, QApplication::UnicodeUTF8));
        leHost->setText(QString());
        lePort->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "Port", 0, QApplication::UnicodeUTF8));
        btnRefresh->setText(QApplication::translate("MainWindow", "Refresh", 0, QApplication::UnicodeUTF8));
        leOldPassword->setText(QString());
        leNewPassword->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "New Password", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Old Password", 0, QApplication::UnicodeUTF8));
        btnChangePassword->setText(QApplication::translate("MainWindow", "Change Password", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "New password must have at least:\n"
"- 6 characters\n"
"- 1 number\n"
"- 1 lower case letter\n"
"- 1 upper case letter", 0, QApplication::UnicodeUTF8));
        lblPasswordError->setText(QString());
        label_8->setText(QApplication::translate("MainWindow", "Retype New Password", 0, QApplication::UnicodeUTF8));
        leRetypeNewPassword->setText(QString());
        btnCancelPWChange->setText(QApplication::translate("MainWindow", "Cancel", 0, QApplication::UnicodeUTF8));
        menuUser->setTitle(QApplication::translate("MainWindow", "User", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
