#-------------------------------------------------
#
# Project created by QtCreator 2012-02-12T19:02:10
#
#-------------------------------------------------

QT       += core gui \ network

TARGET = ClientApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../SpynetCommonFiles/serverinterface.cpp \
    ../SpynetCommonFiles/simplecrypt.cpp

HEADERS  += mainwindow.h \
    ../SpynetCommonFiles/serverinterface.h \
    ../SpynetCommonFiles/simplecrypt.h

FORMS    += mainwindow.ui
