#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../SpynetCommonFiles/serverinterface.h"
#include <QMainWindow>
#include <QListWidgetItem>
#include <QMap>
#include <QList>
#include <QString>
#include <QDateTime>
#include <QMultiMap>
#include "../SpynetCommonFiles/simplecrypt.h"

struct userLog
{
    QString username;
    QString log;
};

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool eventFilter(QObject *, QEvent *);

    QString getResponse();

private:
    Ui::MainWindow *ui;
    void outputLog(QString username, QDateTime time, QString log);
    void updateUserListAndLogs();

    QMultiMap<QDateTime, userLog> m_allLogs;
    QString m_spyname;
    QStringList m_checkedUsers;

    ServerInterface skynetServer;
    SimpleCrypt m_crypt;

public slots:
    void btnLoginClicked();
    void mnuLogoutClicked();
    void mnuPasswordClicked();
    void btnChangePasswordClicked();
    void btnRefreshClicked();
    void btnCancelPWChangeClicked();

    void handleResponse(QString* response);
};

#endif // MAINWINDOW_H
