#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QFile>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//    QFile keyFile("sharedkey.pem");
//    if (!keyFile.exists())
//    {
//        QMessageBox::critical(0,"Critical Error","Error: Missing file \"sharedkey.pem\"",QMessageBox::Abort);
//        return 0;
//    }

    MainWindow w;
    w.show();

    return a.exec();
}
